<h1 id="interest-ing-calculations">Interest-ing Calculations</h1>
<h3 id="john-c.-nash-2023-4-1">John C. Nash</h3>


</h4>2023-04-15</h4>

<p>All of us are affected by the interest charged or earned on money, but few people pay much attention to how the 
calculations are performed. Maybe they should, as there are many traps for the unwary. This article looks at a few 
of the issues and tries to clarify what can happen and how things should be done.</p>
<p>In passing, we note that various religions have had strictures concerning interest, generally under 
the term <em>usury</em>.</p>
<h2 id="simple-interest-calculations-and-rounding">Simple interest calculations and rounding</h2>

<p>The basic calculation of the <strong>interest</strong> <em>I</em>, the rent due on principal money <em>P</em> 
deposited or loaned at rate <em>r</em> is</p>
<p><em>I</em> = <em>P</em> * <em>r</em></p>
<p>Note that <em>r</em> is a fraction, not a percentage. In this article, we will use the symbol <em>R</em> for the 
nominal percentage interest per annum, as that is such a commonly used quantity that it deserves its own symbol. 
However, as we shall see later, this causes a number of difficulties.</p>
<p>A related quantity is the <strong>amount</strong> of money accumulated <em>A</em>:</p>
<p><em>A</em> = (1+<em>r</em>) * <em>P</em></p>
<p>All this seems perfectly straightforward until you actually want your cash. Then you discover that</p>
<ul>
<li>calculations (usually) need to be rounded to the nearest penny (in decimal currencies) before they can be recorded in a ledger;</li>
<li>if you want payment, there may be a smallest coin unit. In Canada this is the nickel (5 cent) piece. In the USA at 
2023, it is still a penny (1 cent). 
See <a href="https://en.wikipedia.org/wiki/Cash_rounding" class="uri">https://en.wikipedia.org/wiki/Cash_rounding</a> for a list 
of different national practices.</li>
</ul>
<p>Rounding is a subject that should be straightforward, but in reality is generally done poorly. Let us define a <strong>round 
to even</strong> algorithm.</p>
<blockquote>
<p>Take the number to be rounded and split it after the last digit position to be kept. Let L be the digit to the left 
of the split and R be the number created by putting a decimal point in front of the digits to the right. If R is less 
than .5, leave L as is. If R is greater than .5, increase L by 1, carrying the increase left as needed. If R is exactly 
.5 (i.e. .500000 etc.), L is increased by 1 if it is odd, left as is if even.</p>
</blockquote>
<p>Examples:</p>
<p>12.345 rounded to 2 decimal places or 4 figures is 12.34</p>
<p>12.345 rounded to 2 digits (0 decimals) is 12</p>
<p>12.345 rounded to 1 decimal (3 digits) is 12.3</p>
<p>12.350 rounded to 1 decimal (3 digits) is 12.4</p>
<p>A serious <strong>ERROR</strong> comes from <strong>cascade rounding</strong>, which I have seen students 
doing, and also arguing that their teachers taught them. This tries to do the following.</p>
<p>Suppose we want to round 1.234567 to 2 decimals (3 digits). The correct answer is to look at .4567 &lt; 0.5 
and round to 1.23.</p>
<p>Cascade rounding tries to do the following:</p>
<pre><code> 1.234567 -&gt; 1.23457 -&gt; 1.2346 -&gt; 1.235 -&gt; 1.24  **WRONG**
 </code></pre>
<p>While round-to-even is sometimes called “Banker’s rounding”, we would argue that it is the most common standard beyond t
he financial industry. However, readers should be aware that calculations may be executed with other rules, including 
<strong>truncation</strong>, which always returns L as the left hand digit regardless of the value of R.</p>
<h2 id="compound-interest">Compound interest</h2>
<p>Compound interest simply means we apply interest calculations repeatedly to a principal amount. For <em>n</em> periods, 
we end up with an amount</p>
<p><em>A</em> = (1+<em>r</em>) * (1+<em>r</em>) * … * (1+<em>r</em>) * <em>P</em> = (1+<em>r</em>)<sup><em>n</em></sup> * <em>P</em></p>
<p>The fun and games arise when we have a real-world bookkeeping situation and must report the amount at each time point where 
interest is accrued. What actually gets recorded will be the result <em>A</em><sub><em>n</em></sub> of a recursion</p>
<p><em>A</em><sub><em>i</em> + 1</sub> = <em>r<strong>o</strong>u<strong>n</strong>d<strong>e</strong>d</em>[(1+<em>r</em>)*<em>A</em><sub><em>i</em></sub>]</p>
<p>where <em>A</em><sub>0</sub> = <em>P</em>.</p>
<p>The rounded version of compound interest can deliver quite different amounts from the all-at-once or textbook formula. Let us explore this with some calculations in R (<a href="https://www.r-project.org" class="uri">https://www.r-project.org</a>).</p>
<pre><code>ci &lt;- function(P, er, n){ # all-at-once compound interest
  # P principal in cents, er effective rate (fraction), n periods
  A &lt;- P * ((1 + er)^n )
}

cir &lt;- function(P, er, n){
  # P principal in cents, er effective rate (fraction), n periods
  A &lt;- P
  for (i in 1:n) {A &lt;- round(A * (1 + er))}
  A  
}

er &lt;- 3/100 # 3 % interest  
n  &lt;- 20  # 20 periods (e.g., years)
pn &lt;- 5
table &lt;- matrix(NA, nrow=5, ncol=5)
for (j in 1:5) {
  P &lt;- 10^j # Try different levels of principal
#  cat(P,&quot; cents at &quot;, 100*er,&quot;%  for &quot;,n,&quot; periods gives &quot;, ci(P, er, n),
#      &quot; all-at-once or&quot;, cir(P, er, n),&quot; rounded\n&quot; )
  table[j,] &lt;- c(P, 100*er,  ci(P, er, n), round( ci(P, er, n)), cir(P, er, n))
}
# put output in a table and have all-at-once rounded
colnames(table)&lt;-c(&quot;P&quot;, &quot;R&quot;, &quot;textbook&quot;, &quot;rd(textbook)&quot;, &quot;rounded&quot; )
print(table)

##          P R     textbook rd(textbook) rounded
## [1,] 1e+01 3     18.06111           18      10
## [2,] 1e+02 3    180.61112          181     179
## [3,] 1e+03 3   1806.11123         1806    1808
## [4,] 1e+04 3  18061.11235        18061   18061
## [5,] 1e+05 3 180611.12347       180611  180613</code></pre>
<p>We see from the first row that it is possible to get no interest at all when we round. Yet simple interest over the 20 periods would give an amount</p>
<p>10 * (1 + (20*3/100) ) = 16 cents.</p>
<p>When interest rates are very low – and at the end of March 2023 TD Canada Trust is offering only 0.01% on savings accounts – the effects of rounding can be to eliminate all interest. As far as we can tell, this low rate is the per-annum rate (i.e., <em>R</em>) and not the per-day rate (i.e., 100 * <em>r</em>) in our description.</p>
<h2 id="minimum-balance-to-earn-daily-interest-over-a-365-day-year">Minimum balance to earn daily interest over a 365 day year</h2>
<p>Clearly, once rounding is in effect, we can have quite a lot of money in the bank and earn no interest, even on an account advertised as “Compound daily interest savings”. Moreover, the effect of rounding is such that a balance difference of 1 cent at the beginning of a year makes a great difference in the end-of-year balance. Let us explore this.</p>
<pre><code>crdamt &lt;- function(B, er, divn){ # Compound rounded interest for &quot;year&quot;
# B = balance in cents, er = fractional interest rate for &quot;year&quot;, divn periods
  A &lt;- B # amount in cents
  xer &lt;- 0.01*er/divn
  for (i in 1:divn){
     A &lt;- round(A*(1+xer))
  }
  A # amount in cents (smallest currency unit)
}

cmieb &lt;- function(er, divn){ # Minimum interest earning balance
  xer &lt;- 0.01*er/divn
  low &lt;- 0 # lower bound
  up &lt;- round(1/xer) # trial upper bound
  while (crdamt(up, er, divn) - up &lt;= 0) {
     up &lt;- 2*up+1 # ; cat(&quot;up=&quot;,up,&quot;\n&quot;) # include if log wanted
  }
  count &lt;- 1
  while ( up - low &gt; 1 ) { #    cat(&quot;low and up&quot;, low, up,&quot;\n&quot;)
     xx &lt;- round(0.5*(low+up))
     vv &lt;- crdamt(xx, er, divn) - xx
     if (vv == 0) { low &lt;- xx }
     else {up &lt;- xx} # choose up &lt;- xx
     if (count &gt;100) break # emergency exit
     count &lt;- count+1
  }
  BC &lt;- up # minimum balance that earns interest
}

divn &lt;- 365
pcrv &lt;- seq(0.01, 5, 0.01)
n &lt;- length(pcrv)
balv&lt;-rep(NA, n)
valint&lt;-rep(NA, n)
for (i in 1:n){
  rate &lt;- pcrv[i]
  balv[i] &lt;- cmieb(rate, divn)
  valint[i] &lt;- crdamt(balv[i], rate, divn)-balv[i]
}
plot(pcrv, balv)</code></pre>
<p><img src="IC_files/figure-markdown_strict/minbalX-1.png" /></p>
<p>It turns out that the smallest non-zero interest paid will be a penny a day, or 365 cents, and this will be paid on the minimum interest earning balance. We note that this balance is 1825000 cents or $18,250 at the 0.01% interest rate quoted above. The minimum interest earning balance does not drop below $1000 until we are offered at least 0.19% per annum on a daily interest account. Why bother?!</p>
<h2 id="calendar-annoyances">Calendar annoyances</h2>
<p>Calendars are a human construct to organize time. While the peculiarities of our solar system account for a lot of the difficulties with time division for interest calculations, humans have layered on more confusion.</p>
<p>Months can be from 28 to 31 days. This means a “half year” can be from 181 to 184 days. Of course, a 182.5 day half-year is equally awkward.</p>
<p>Generally monthly payments are made on the same numbered day of each month, even if the elapsed time between payments is unequal. The first of the month is a common choice. Note that the last day of the month gives different day-of-month numbers, but could be a choice.</p>
<p>A further complication is that we may want the first or last <strong>business day</strong> of the month. National, provincial/state or even municipal holidays then intervene to complicate matters.</p>
<h2 id="loan-amortization">Loan amortization</h2>
<p>Loans are <strong>amortized</strong> or paid off in various ways. The most common and easiest to understand is the loan of principal <em>P</em><sub>0</sub> at effective interest of 100 * <em>r</em>% per period for <em>n</em> periods. We can work out the per-period payment <em>z</em>.</p>
<p>First, in each period,</p>
<p><em>P</em><sub><em>i</em> + 1</sub> = <em>P</em><sub><em>i</em></sub> * (1+<em>r</em>) − <em>z</em> or more strictly</p>
<p><em>P</em><sub><em>i</em> + 1</sub> = <em>r<strong>o</strong>u<strong>n</strong>d<strong>e</strong>d</em>(<em>P</em><sub><em>i</em></sub>*(1+<em>r</em>)−<em>z</em>) For now ignore the rounding. We want to find <em>z</em> so that <em>n</em> payments give <em>P</em><sub><em>n</em></sub> = 0. But this is</p>
<p><em>P</em><sub><em>n</em></sub> = 0 = <em>P</em><sub>0</sub> * (1+<em>r</em>)<sup><em>n</em></sup> − <em>z</em> * ∑<sub><em>i</em> = 1, <em>n</em></sub>((1+<em>r</em>)<sup>(<em>i</em>−1)</sup>) The right hand summation is a geometric progression. Its sum has a known formula, which we can easily derive.</p>
<p><em>Q</em> = ∑<sub><em>i</em> = 1, <em>n</em></sub>((1+<em>r</em>)<sup><em>i</em></sup> <em>Q</em> * ((1+<em>r</em>)−1) = <em>r</em> * <em>Q</em> = (1+<em>r</em>)<sup><em>n</em></sup> − 1) <em>Q</em> = [(1+<em>r</em>)<sup><em>n</em></sup>−1]/<em>r</em> Giving</p>
<p><em>P</em><sub>0</sub> * (1+<em>r</em>)<sup><em>n</em></sup> * <em>r</em>/[(1+<em>r</em>)<sup><em>n</em></sup>−1] = <em>z</em> or</p>
<p><em>z</em> = <em>P</em><sub>0</sub> * <em>r</em>/[1−(1+<em>r</em>)<sup>−<em>n</em></sup>] Thus the per-period payment for a loan of $10000 for 20 periods at 2% is</p>
<pre><code>z &lt;- 10000*0.02/(1 - 1.02^(-20))
z

## [1] 611.5672</code></pre>
<h2 id="irregular-amortization">Irregular amortization</h2>
<p>Note that the usual convention is that payment is due at the END of each period, though there are many variations, and a lot of “pay-day” loans seem to front load the interest part of the payment at least, though they charge interest for the whole of the period.</p>
<p>Furthermore, rounding, business days or closings, delayed or early payments, extra or balloon payments, variable rate 
interest, or differing amortization and contract periods may alter the amount owing. Nevertheless, we can write the algorithm for the balance owing in full generality as</p>
<p>New_Principal = Payment_rule(Principal, rate(s), time_events)</p>
<p>If we slightly simplify so the rate for the period is <em>r</em><sub><em>i</em></sub> (in fractional form, not percentage, 
and applicable to the payment period), <em>z</em><sub><em>i</em></sub> is the current payment and <em>f</em><sub><em>i</em></sub> is the fraction of a regular payment period since the last reconciliation, then we get a new balance of</p>
<p><em>P</em><sub><em>i</em> + 1</sub> = <em>r<strong>o</strong>u<strong>n</strong>d<strong>e</strong>d</em>(<em>f</em><sub><em>i</em></sub>*<em>P</em><sub><em>i</em></sub>*(1+<em>r</em>)−<em>z</em><sub><em>i</em></sub>) Note that <em>f</em><sub><em>i</em></sub> is 1 for a regular payment interval, but could be larger or smaller depending on holidays, calendar effects, etc. Traditionally we did not see values for <em>f</em><sub><em>i</em></sub> other than 1. Similarly <em>z</em><sub><em>i</em></sub> was usually a fixed value approximated by the formula above for the per-period payment based on the number of amortization periods.</p>
<h2 id="contracts-shorter-than-amortization-period">Contracts shorter than amortization period</h2>
<p>The United States has commonly used mortgages that are contracted for the entire amortization period, typically 25 years or 300 months, with <em>r</em> = <em>R</em>/1200. British banks have been wary of intererst rate changes, and have treated mortgages as <strong>demand loans</strong> which are callable at any time. This would lead to a very disruptive process if people had to find ways to pay back the remaining mortgage balance at the whim of the bank. In practice, when rates change, the residue is computed i.e., a zero payment is presumed, then the next payment is made at a new rate for the (partial) period. Usually the bank communicates a different payment to the borrower.</p>
<p>Canada is, as usual, in between. Though variable rate mortgages are quite common (the situation just described for the UK, though likely using a modified narrative) fixed-rate mortgages in Canada generally have a shorter <strong>contract period</strong>, say 3 or 5 years, while the <strong>amortization period</strong> is 25 to 30 years. At the end of the contract, the borrower must repay the balance, typically by re-mortgaging a property i.e., recontracting the loan.</p>
<h2 id="more-canadian-quirks">More Canadian quirks</h2>
<p>A further oddity in Canada is that the Canada Interest Act (<a href="https://laws-lois.justice.gc.ca/eng/acts/i-15/page-1.html#h-270664" class="uri">https://laws-lois.justice.gc.ca/eng/acts/i-15/page-1.html#h-270664</a>) prescribes that mortgages shall be payable semi-annually or annually, not in advance. Of course, most householders prefer to pay monthly. This has led to a peculiar computation game that I believe is unique to Canada.</p>
<p>A way to think of this is that the lender (e.g., bank) accepts monthly payments and treats them as if they were deposits to a savings account at a fractional rate <em>r</em> that is such that the accumulated amount will be equal to the semi-annual payment.</p>
<p>Ignoring rounding, this is the same as using a monthly rate</p>
<p><em>r</em> = (1+<em>R</em>/200)<sup>1/6</sup> − 1 We can check this with an example.</p>
<pre><code># semi-annual payment at 4% for 20 years on $100,000
z &lt;- 100000*0.02/(1 - 1.02^(-40))
z # the payment value (unrounded)

## [1] 3655.575

r &lt;- (1.02)^(1/6) - 1 # effective monthly rate
r # value

## [1] 0.00330589

# monthly payment at this rate on $100,000 for 20 years=240 months
q &lt;- 100000*r/(1-(1+r)^(-240)) 
q # value

## [1] 604.2465

40 * z # total payout semi-annually 

## [1] 146223

240*q # total monthly pmts. Smaller because of earlier payment.

## [1] 145019.2

# check that value of 6 monthly payments is same as semi-annual
# payments calculated in reverse order of submission
# 0 months, 1, 2, 3, 4, 5 months interest earned
q+q*(1+r)+q*(1+r)^2+q*(1+r)^3+q*(1+r)^4+q*(1+r)^5

## [1] 3655.575

z # note the same value as computed before

## [1] 3655.575</code></pre>
<p>We will not get into rounded computations here, but they are important when real mortgages in Canada are contracted. Unless the lender provides the borrower with a schedule of payments, the “rules” fall back to semi-annual or annual payments with a maximum rate prescribed at 5%.</p>
<h2 id="weekly-mortgages">Weekly mortgages</h2>
<p>There have been occasional fashions for weekly mortgages. These present yet another issue, particularly in Canada. First, several reasonable mechanisms for the computation of the effective per period fractional rate <em>r</em> can be proposed. In the mid-1980s I did discover at least 2 ways used by different financial institutions. Second, there are calendar issues in reconciling the weekly payments with “semi-annual”.</p>
<h2 id="a-possible-set-of-rules">A possible set of rules</h2>
<p>As mentioned, financial institutions may put forth many different rules. Most are likely acceptable, but some are almost certainly unfair to borrowers in that the conditions and calculation algorithms are not clearly laid out. Here are some specifications I would recommend.</p>
<ul>
<li>interest rates should be stated in per-annum values <em>R</em> with documented algorithms for computing the effective per-period fractional rate <em>r</em>.</li>
<li>the number of compounding periods in a year should be given clearly</li>
<li>the timing of payments should be specified, if necessary including the time zone, along with rules for dealing with holidays or business closing days, as well as exceptions.</li>
<li>for partial periods, simple interest on the fractional period should be used, that is, the interest due should be $ P * r * (f) $ where <em>f</em> is the fraction of the period.</li>
<li>interest is paid or due at the end of each payment period i.e., not in advance.</li>
</ul>
